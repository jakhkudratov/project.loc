<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TeachScience */

$this->title = 'Update Teach Science: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Teach Sciences', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="teach-science-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'sciences' => $sciences,
        'teachers' => $teachers
    ]) ?>

</div>

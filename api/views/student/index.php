<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Groups;

/* @var $this yii\web\View */
/* @var $searchModel common\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Student', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]);
        $groups = new Groups();

     ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'full_name',
            'username',
            //'group_id',
            //'user_id',
            [
                'attribute'=> 'group_id',
                'value' => 'group.group_namber',
                'format' => 'raw'
            ],
            [
                'attribute'=> 'user_id',
                'value' => 'user.username',
                'format' => 'raw'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],

        
    ]); ?>


</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ScienceTableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Science Tables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="science-table-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Science Table', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'room_id',
            //'lesson_id',
            //'para_id',
            //'week_day_id',
            [
                'attribute'=> 'room_id',
                'value' => 'room.room_namber',
                'format' => 'raw'
            ],
            [
                'attribute'=> 'lesson_id',
                'value' => 'lesson.lesson_name',
                'format' => 'raw'
            ],
            [
                'attribute'=> 'para_id',
                'value' => 'para.number_para',
                'format' => 'raw'
            ],
            [
                'attribute'=> 'week_day_id',
                'value' => 'weekDay.day_name',
                'format' => 'raw'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

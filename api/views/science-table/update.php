<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ScienceTable */

$this->title = 'Update Science Table: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Science Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="science-table-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'roomSelect' => $roomSelect,
        'rooms' => $rooms,
        'lessonSelect' => $lessonSelect,
        'lessons' => $lessons, 
        'paras' => $paras,
        'paraSelect' => $paraSelect,
        'weekDaySelect' => $weekDaySelect,
        'weekDays' => $weekDays,
        
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ScienceTable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="science-table-form">

    <?php $form = ActiveForm::begin(); ?>
    <h2>Xonani tanlang</h2>
    <?= Html::dropDownList('room_id', $roomSelect, $rooms, ['class' => 'form-control']) ?>

	<h2>Darsni tanlang</h2>    
    <?= Html::dropDownList('lesson_id', $lessonSelect, $lessons, ['class' => 'form-control']) ?>

    <h2>Darsni parasini tanlang</h2>    
    <?= Html::dropDownList('para_id', $paraSelect, $paras, ['class' => 'form-control']) ?>


    <h2>Xafta kunini tanlang</h2>    
    <?= Html::dropDownList('week_day_id', $weekDaySelect, $weekDays, ['class' => 'form-control']) ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

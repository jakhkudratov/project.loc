<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\Lesson */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="lesson-form">

    <?php $form = ActiveForm::begin(); ?>
    <h2>Fan o'qituvchisi va fanni tanlang</h2>
    <?= Html::dropDownList('teach_science_id', $teachScienceSelect, $teachSciences, ['class' => 'form-control']) ?>
    <h2>Guruhni tanlang</h2>

    <?= Html::dropDownList('group_id', $groupSelect, $groups, ['class' => 'form-control']) ?>
    <? 
    	/*echo Select2::widget([
    		'model' => $model,
    		'attribute' => 'group_id',
    		'data' => $groups 
    	]);*/
    ?>


    <div class="form-group mt-2">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

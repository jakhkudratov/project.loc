<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CafedrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cafedries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cafedry-index">


    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Cafedry', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'cafedry_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    Pjax::end();
    ?>


</div>

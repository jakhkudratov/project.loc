<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
        //'public/css/bootstrap.min.css',
        //'public/css/dashboard.css',

    ];
    public $js = [
        //'js/jquery-3.3.1.min.js',
        //'js/dashboard.js'

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

<?php

namespace backend\controllers;

use Yii;
use common\models\ScienceTable;
use common\models\ScienceTableSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\Para;
use common\models\WeekDays;
use common\models\Lesson;
use common\models\Rooms;
use yii\filters\AccessControl;

/**
 * ScienceTableController implements the CRUD actions for ScienceTable model.
 */
class ScienceTableController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],

            ],
            [
               'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    
                ], 
            ],
        ];
    }

    /**
     * Lists all ScienceTable models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new ScienceTableSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ScienceTable model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ScienceTable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ScienceTable();

        $lessons = ArrayHelper::map(Lesson::find()->all(), 'id', 'lesson_name');
        $paras = ArrayHelper::map(Para::find()->all(), 'id', 'number_para');
        $weekDays = ArrayHelper::map(WeekDays::find()->all(), 'id', 'day_name');
        $rooms = ArrayHelper::map(Rooms::find()->all(), 'id', 'room_namber');

        if (!empty(Yii::$app->request->post())) {
            $model->room_id = Yii::$app->request->post()['room_id'];
            $model->week_day_id = Yii::$app->request->post()['week_day_id'];
            $model->lesson_id = Yii::$app->request->post()['lesson_id'];
            $model->para_id = Yii::$app->request->post()['para_id'];
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'rooms' => $rooms,
            'lessons' => $lessons, 
            'paras' => $paras,
            'weekDays' => $weekDays,
        ]);
    }

    /**
     * Updates an existing ScienceTable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $lessons = ArrayHelper::map(Lesson::find()->all(), 'id', 'science_name');
        $paras = ArrayHelper::map(Para::find()->all(), 'id', 'full_name');
        $weekDays = ArrayHelper::map(WeekDays::find()->all(), 'id', 'science_name');
        $rooms = ArrayHelper::map(Rooms::find()->all(), 'id', 'room_namber');

        if (!empty(Yii::$app->request->post())) {
            $model->room_id = Yii::$app->request->post()['room_id'];
            $model->week_day_id = Yii::$app->request->post()['week_day_id'];
            $model->lesson_id = Yii::$app->request->post()['lesson_id'];
            $model->para_id = Yii::$app->request->post()['para_id'];
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'roomSelect' => $model->room_id,
            'rooms' => $rooms,
            'lessonSelect' => $model->lesson_id,
            'lessons' => $lessons, 
            'paras' => $paras,
            'paraSelect' => $model->para_id,
            'weekDaySelect' => $model->week_day_id,
            'weekDays' => $weekDays,
        ]);
    }

    /**
     * Deletes an existing ScienceTable model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ScienceTable model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScienceTable the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ScienceTable::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace backend\controllers;

use Yii;
use common\models\Lesson;
use common\models\LessonSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\Groups;
use common\models\TeachScience;
use yii\filters\AccessControl;
/**
 * LessonController implements the CRUD actions for Lesson model.
 */
class LessonController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            [
               'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    
                ], 
            ],
        ];
    }

    /**
     * Lists all Lesson models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LessonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Lesson model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Lesson model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Lesson();

        $teachSciences = ArrayHelper::map(TeachScience::find()->all(), 'id', 'full_name');
        $groups = ArrayHelper::map(Groups::find()->all(), 'id', 'group_namber');

        if (!empty(Yii::$app->request->post())) {
            $model->teach_science_id = Yii::$app->request->post()['teach_science_id'];
            $model->group_id = Yii::$app->request->post()['group_id'];
            $TeachScience = TeachScience::findOne($model->teach_science_id);
            $groups = Groups::findOne($model->group_id);
            $model->lesson_name = $TeachScience->full_name . '_' . $groups->group_namber;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'teachSciences' => $teachSciences,
            'groups' => $groups,
            'teachScienceSelect' => 0,
            'groupSelect' => 0
        ]);
    }

    /**
     * Updates an existing Lesson model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        
        $teachSciences = ArrayHelper::map(TeachScience::find()->all(), 'id', 'full_name');
        $groups = ArrayHelper::map(Groups::find()->all(), 'id', 'group_namber');

        if (!empty(Yii::$app->request->post())) {
            $model->teach_science_id = Yii::$app->request->post()['teach_science_id'];
            $model->group_id = Yii::$app->request->post()['group_id'];
            $TeachScience = TeachScience::findOne($model->teach_science_id);
            $groups = Groups::findOne($model->group_id);
            $model->lesson_name = $TeachScience->full_name . '_' . $groups->group_namber;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'teachSciences' => $teachSciences,
            'groups' => $groups,
            'teachScienceSelect' => $model->teach_science_id,
            'groupSelect' => $model->group_id
        ]);
    }

    /**
     * Deletes an existing Lesson model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Lesson model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lesson the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lesson::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m210312_182917_modify_sidnals_table
 */
class m210312_182917_modify_sidnals_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\Signals::tableName(), 'group_id', $this->integer());
        $this->createIndex(
            'index-signals-group_id',
            \backend\models\Signals::tableName(),
            'group_id'
        );

        $this->addForeignKey(
            'fk-signals-group_id',
            'signals',
            'group_id',
            'signal_group',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210312_182917_modify_sidnals_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210312_182917_modify_sidnals_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m210316_183611_modify_user_table
 */
class m210316_183611_modify_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\models\User::tableName(), 'role', $this->integer());
        $this->addColumn(\common\models\User::tableName(), 'verification_token', $this->integer());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210316_183611_modify_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210316_183611_modify_user_table cannot be reverted.\n";

        return false;
    }
    */
}

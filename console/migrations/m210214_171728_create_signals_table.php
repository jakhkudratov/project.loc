<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%signals}}`.
 */
class m210214_171728_create_signals_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%signals}}', [
            'id' => $this->primaryKey(),
            'time' => $this->float(),
            'amplituda' => $this->float(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%signals}}');
    }
}

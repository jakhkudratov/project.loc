<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.10.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.17.1.0',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'cebe/yii2-gravatar' => 
  array (
    'name' => 'cebe/yii2-gravatar',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@cebe/gravatar' => $vendorDir . '/cebe/yii2-gravatar/cebe/gravatar',
    ),
  ),
  'dmstr/yii2-adminlte-asset' => 
  array (
    'name' => 'dmstr/yii2-adminlte-asset',
    'version' => '2.6.2.0',
    'alias' => 
    array (
      '@dmstr' => $vendorDir . '/dmstr/yii2-adminlte-asset',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base/src',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '2.2.0.0',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2/src',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '1.5.8.0',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform/src',
    ),
  ),
  'kartik-v/yii2-sortable' => 
  array (
    'name' => 'kartik-v/yii2-sortable',
    'version' => '1.2.2.0',
    'alias' => 
    array (
      '@kartik/sortable' => $vendorDir . '/kartik-v/yii2-sortable/src',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog/src',
    ),
  ),
  'kartik-v/yii2-bootstrap4-dropdown' => 
  array (
    'name' => 'kartik-v/yii2-bootstrap4-dropdown',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/bs4dropdown' => $vendorDir . '/kartik-v/yii2-bootstrap4-dropdown/src',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.3.5.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid/src',
    ),
  ),
  'kartik-v/yii2-dynagrid' => 
  array (
    'name' => 'kartik-v/yii2-dynagrid',
    'version' => '1.5.1.0',
    'alias' => 
    array (
      '@kartik/dynagrid' => $vendorDir . '/kartik-v/yii2-dynagrid/src',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => '1.4.7.0',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker/src',
    ),
  ),
  'kartik-v/yii2-widget-timepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-timepicker',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@kartik/time' => $vendorDir . '/kartik-v/yii2-widget-timepicker/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  '2amigos/yii2-arrayquery-component' => 
  array (
    'name' => '2amigos/yii2-arrayquery-component',
    'version' => '1.0.3.0',
    'alias' => 
    array (
      '@dosamigos/arrayquery' => $vendorDir . '/2amigos/yii2-arrayquery-component/src',
    ),
  ),
  'yii2mod/yii2-rbac' => 
  array (
    'name' => 'yii2mod/yii2-rbac',
    'version' => '2.3.0.0',
    'alias' => 
    array (
      '@yii2mod/rbac' => $vendorDir . '/yii2mod/yii2-rbac',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.1.2.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.1.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.1.16.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'deesoft/yii2-angular' => 
  array (
    'name' => 'deesoft/yii2-angular',
    'version' => '1.3.0.0',
    'alias' => 
    array (
      '@dee/angular' => $vendorDir . '/deesoft/yii2-angular',
    ),
    'bootstrap' => 'dee\\angular\\Bootstrap',
  ),
  'deesoft/yii2-adminlte' => 
  array (
    'name' => 'deesoft/yii2-adminlte',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@dee/adminlte' => $vendorDir . '/deesoft/yii2-adminlte',
    ),
  ),
  'mdmsoft/yii2-admin' => 
  array (
    'name' => 'mdmsoft/yii2-admin',
    'version' => '3.2.0.0',
    'alias' => 
    array (
      '@mdm/admin' => $vendorDir . '/mdmsoft/yii2-admin',
    ),
  ),
  'wbraganca/yii2-fancytree-widget' => 
  array (
    'name' => 'wbraganca/yii2-fancytree-widget',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@wbraganca/fancytree' => $vendorDir . '/wbraganca/yii2-fancytree-widget/src',
    ),
  ),
  'mihaildev/yii2-ckeditor' => 
  array (
    'name' => 'mihaildev/yii2-ckeditor',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@mihaildev/ckeditor' => $vendorDir . '/mihaildev/yii2-ckeditor',
    ),
  ),
  'mihaildev/yii2-elfinder' => 
  array (
    'name' => 'mihaildev/yii2-elfinder',
    'version' => '1.4.0.0',
    'alias' => 
    array (
      '@mihaildev/elfinder' => $vendorDir . '/mihaildev/yii2-elfinder',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput/src',
    ),
  ),
  'yiisoft/yii2-bootstrap4' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap4',
    'version' => '2.0.9.0',
    'alias' => 
    array (
      '@yii/bootstrap4' => $vendorDir . '/yiisoft/yii2-bootstrap4/src',
    ),
  ),
);

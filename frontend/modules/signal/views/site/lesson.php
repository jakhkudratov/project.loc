<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;

/* @var $this yii\web\View */

$this->title = 'My university';

?>


<div class="container">
    <div class="row">
        <div class="col-md-3">
            <h2>O'qituvchi</h2>
            <?
                echo $data['teacher'];
            ?>
        </div>

        <div class="col-md-3">
            <h2>Guruh</h2>
            <?
                echo $data['group'];
            ?>
        </div>    

        <div class="col-md-3">
            <h2>Fan</h2>
            <?
                echo $data['science'];
            ?>
        </div>     
    </div>
</div>
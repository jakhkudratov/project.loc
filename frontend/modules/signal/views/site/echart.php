<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SignalsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Signals';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="signals-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



</div>


<?php
$js = <<<JS

sendAjaxToCart();


function sendAjaxToCart(){
    $.ajax({
        //method: "POST",
        url: "/site/empg", 
        success: function (response){
            //console.log(response);
            if (!response === false){
                painChart(response['data'], response['xAxis'], response['yAxis'])
                //console.log(response)
            }
        }

    });
}



function painChart(data, xAxis, yAxis)
{
    var x = Object.keys(xAxis).map(function (_) { return xAxis[_];  })
    x = Object.values(xAxis);
    var y = Object.keys(yAxis).map(function (_) { return xAxis[_];  })
    y = Object.values(yAxis);
    
    var dom = document.getElementById("main");
    var myChart = echarts.init(dom);
    var app = {};
    option = null;
    option = {
        xAxis: {
            type: 'category',
            data: x
        },
        yAxis: {
            type: 'value'
        },
        series: [{
            data: y,
            type: 'line'
        }]
    };
  
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}
 
    
     
    am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_animated);
        am4core.useTheme(am4themes_material);
// Themes end

// Create chart instance
        var chart = am4core.create("chartdiv_2", am4charts.XYChart);
          
// Add data
        chart.data = data

// Create axes
        var xAxis = chart.xAxes.push(new am4charts.ValueAxis());
        xAxis.renderer.minGridDistance = 40;

// Create value axis
        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueX = "key";
        series1.dataFields.valueY = "emp";
        chart.scrollbarX = new am4charts.XYChartScrollbar();
        chart.scrollbarX.series.push(series1);
        
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.xAxis = xAxis;
        chart.cursor.snapToSeries = series1;

    });
   

}



JS;
$this->registerJs($js);
?>
<!-- Styles -->
<style>
    #chartdiv_ {
        width: 100%;
        height: 500px;
        max-width: 100%;
        background-color: white;
    }
    #chartdiv_2 {
        width: 100%;
        height: 500px;
        max-width: 100%;
        background-color: white;
    }
    .back{
        background-color: white;
    }

</style>



<!-- HTML -->
<div class="back">


    <h2>IEMG</h2>

    <div id="chartdiv_2" style="height: 500px"></div>
    <div id="main" style="width: 100%; height: 300px">

    </div>
</div>

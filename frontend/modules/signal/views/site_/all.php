<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SignalsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Signals';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="signals-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



</div>


<?php
$js = <<<JS

sendAjaxToCart();


function sendAjaxToCart(){
    $.ajax({
        //method: "POST",
        url: "/site/first-signal", 
        success: function (response){
            //console.log(response);
            if (!response === false){
                painChart(response['new_data'])
                //console.log(response)
            }
        }

    });
}



function painChart(new_data)
{
    am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
        var chart = am4core.create("chartdiv_", am4charts.XYChart);

// Add data
        chart.data = new_data

// Create axes
        var xAxis = chart.xAxes.push(new am4charts.ValueAxis());
        xAxis.renderer.minGridDistance = 40;

// Create value axis
        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueX = "time";
        series1.dataFields.valueY = "amplituda";

        //var bullet1 = series1.bullets.push(new am4charts.CircleBullet());
        /*series1.heatRules.push({
            target: bullet1.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet1.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";

        /*var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueX = "x";
        series2.dataFields.valueY = "by";
        series2.dataFields.value = "bValue";
        series2.strokeWidth = 2;*/

       // var bullet2 = series2.bullets.push(new am4charts.CircleBullet());
        /*series2.heatRules.push({
            target: bullet2.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet2.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";



// Add scrollbar
chart.scrollbarX = new am4charts.XYChartScrollbar();
chart.scrollbarX.series.push(series1);

// Add cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.xAxis = xAxis;
chart.cursor.snapToSeries = series1;

//scrollbars
       /* chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarY = new am4core.Scrollbar();*/

    });
    

}



JS;
$this->registerJs($js);
?>
<!-- Styles -->
<style>
    #chartdiv_ {
        width: 100%;
        height: 500px;
        max-width: 100%;
        background-color: white;
    }
    #chartdiv_2 {
        width: 100%;
        height: 500px;
        max-width: 100%;
        background-color: white;
    }
    .back{
        background-color: white;
    }

</style>



<!-- HTML -->
<div class="back">

    <h2>First Signal</h2>
    <div id="chartdiv_" style="height: 800px"></div>

</div>

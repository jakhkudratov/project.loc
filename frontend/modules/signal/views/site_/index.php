<?php

use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SignalsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Signals';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="signals-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php
    Modal::begin([
        'header' => '<h4>' . 'Import Excel'. '</h4>',
        'toggleButton' => [
            'label' => 'Import Excel',
            'class' => 'btn btn-warning'
        ],
    ]);


    $this->registerJs("
                    var uploas, uploadedFiles = {}, deletedFiles = [],
                    uploaded = document.getElementById('upload_files'),
                    deleted = document.getElementById('deleted_images');");

    echo Html::beginForm(['image-import'], 'POST')
        .Html::hiddenInput( 'name_zip', null,['id' => 'upload_files'])

        .Select2::widget([
                'name' => 'user_id',
                'data' => \common\models\User::getAllUsers(),
                'options' => [
                    'placeholder' => 'Select user ...',
                ],
            ])
        . FileInput::widget([
            'name' => 'file',
            'options' => [
                'multiple' => true
            ],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['upload-file']),
                'deleteUrl' => Url::to(['delete-image']),
                //'allowedPreviewExtensions' => ['jpg', 'jpeg', 'png', 'svg', 'zip', 'rar', 'mp4', 'avi', 'mkv'],
                //'maxFileSize' => 10240,
                'maxFileCount' => 1,
                'fileActionSettings' => [
                    'removeIcon' => '<i class="fas fa-trash"></i>',
                    'uploadIcon' => '<i class="fas fa-file-upload"></i>',
                    'zoomIcon' => '<i class="fas fa-search-plus"></i>'
                ]
            ],
            'pluginEvents' => [
                'fileuploaded' => new JsExpression('function(event, data, previewId) {
                                    console.log(data.response);
                                    uploadedFiles[data.response.name] = data.response.path;
        
                                    uploaded.value = data.response.path;
                                    
                                    console.log(uploaded.value);
                                }'),
                'filedeleted' => new JsExpression('function(event, key) {
                                    deletedFiles.push(key);
                                    deleted.value = JSON.stringify(deletedFiles);
                                }'),
                'filesuccessremove' => new JsExpression('function(event, previewId) {
                                    delete uploadedFiles[previewId];
                                    uploaded.value = JSON.stringify(uploadedFiles);
                                }'),

            ]
        ])
        . '<div class="input-group-btn">'
        . Html::submitButton('Send', ['class' => 'btn btn-success'])
        . '</div>'
        . Html::endForm();



    Modal::end();
    ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



</div>


<?php
$js = <<<JS

sendAjaxToCart();


function sendAjaxToCart(){
    $.ajax({
        //method: "POST",
        url: "/site/data", 
        success: function (response){
            //console.log(response);
            if (!response === false){
                console.log(response['new_data'])
                console.log(response['data'])
                painChart(response['xAxis'], response['yAxis'], response['data'], response['new_data'], response['mav'], response['data_ssi'], response['data_rms'])
                //console.log(response)
            }
        }

    });
}



function painChart(xAxis, yAxis, data, new_data, data_mav, data_ssi, data_rms)
{
    /*var x = Object.keys(xAxis).map(function (_) { return xAxis[_];  })
    x = Object.values(xAxis);
    var y = Object.keys(yAxis).map(function (_) { return xAxis[_];  })
    y = Object.values(yAxis);
    //console.log(data)
    var my_data = Object.keys(data).map(function (_) { return data[_];  })
    my_data = Object.values(data);
    //console.log(my_data)
    
    var dom = document.getElementById("main");
    var myChart = echarts.init(dom);
    var app = {};
    option = null;
    option = {
        xAxis: {
            type: 'category',
            data: x
        },
        yAxis: {
            type: 'value'
        },
        series: [{
            data: y,
            type: 'line'
        }]
    };
option_ = {
    title: {
        text: 'text multi'
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data: ['1', '2']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: x
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name: '1',
            type: 'line',
            stack: '10',
            data: y
        },
        {
            name: '2',
            type: 'line',
            stack: '10',
            data: my_data
        }
    ]
    
};    
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}


var dom_ = document.getElementById("main_");
    var myChart_ = echarts.init(dom_);
    myChart_.setOption(option_, true)*/
    
    
     
    am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
        var chart = am4core.create("chartdiv_", am4charts.XYChart);

// Add data
        chart.data = new_data

// Create axes
        var xAxis = chart.xAxes.push(new am4charts.ValueAxis());
        xAxis.renderer.minGridDistance = 40;

// Create value axis
        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueX = "time";
        series1.dataFields.valueY = "amplituda";

        //var bullet1 = series1.bullets.push(new am4charts.CircleBullet());
        /*series1.heatRules.push({
            target: bullet1.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet1.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";

        /*var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueX = "x";
        series2.dataFields.valueY = "by";
        series2.dataFields.value = "bValue";
        series2.strokeWidth = 2;*/

       // var bullet2 = series2.bullets.push(new am4charts.CircleBullet());
        /*series2.heatRules.push({
            target: bullet2.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet2.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";



// Add scrollbar
chart.scrollbarX = new am4charts.XYChartScrollbar();
chart.scrollbarX.series.push(series1);

// Add cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.xAxis = xAxis;
chart.cursor.snapToSeries = series1;

//scrollbars
       /* chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarY = new am4core.Scrollbar();*/

    });
    am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
        var chart = am4core.create("chartdiv_2", am4charts.XYChart);
          
// Add data
        chart.data = data

// Create axes
        var xAxis = chart.xAxes.push(new am4charts.ValueAxis());
        xAxis.renderer.minGridDistance = 40;

// Create value axis
        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueX = "key";
        series1.dataFields.valueY = "emp";

        //var bullet1 = series1.bullets.push(new am4charts.CircleBullet());
        /*series1.heatRules.push({
            target: bullet1.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet1.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";

        /*var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueX = "x";
        series2.dataFields.valueY = "by";
        series2.dataFields.value = "bValue";
        series2.strokeWidth = 2;*/

       // var bullet2 = series2.bullets.push(new am4charts.CircleBullet());
        /*series2.heatRules.push({
            target: bullet2.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet2.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";

//scrollbars

// Add scrollbar
chart.scrollbarX = new am4charts.XYChartScrollbar();
chart.scrollbarX.series.push(series1);

// Add cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.xAxis = xAxis;
chart.cursor.snapToSeries = series1;
        /*chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarY = new am4core.Scrollbar();*/

    });
    am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
        var chart = am4core.create("chartdiv_3", am4charts.XYChart);
          
// Add data
        chart.data = data_mav

// Create axes
        var xAxis = chart.xAxes.push(new am4charts.ValueAxis());
        xAxis.renderer.minGridDistance = 40;

// Create value axis
        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueX = "key";
        series1.dataFields.valueY = "emp";

        //var bullet1 = series1.bullets.push(new am4charts.CircleBullet());
        /*series1.heatRules.push({
            target: bullet1.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet1.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";

        /*var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueX = "x";
        series2.dataFields.valueY = "by";
        series2.dataFields.value = "bValue";
        series2.strokeWidth = 2;*/

       // var bullet2 = series2.bullets.push(new am4charts.CircleBullet());
        /*series2.heatRules.push({
            target: bullet2.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet2.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";

//scrollbars

// Add scrollbar
chart.scrollbarX = new am4charts.XYChartScrollbar();
chart.scrollbarX.series.push(series1);

// Add cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.xAxis = xAxis;
chart.cursor.snapToSeries = series1;
        /*chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarY = new am4core.Scrollbar();*/

    });
    am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
        var chart = am4core.create("chartdiv_4", am4charts.XYChart);
          
// Add data
        chart.data = data_ssi
    
// Create axes
        var xAxis = chart.xAxes.push(new am4charts.ValueAxis());
        xAxis.renderer.minGridDistance = 40;

// Create value axis
        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueX = "key";
        series1.dataFields.valueY = "ssi";

        //var bullet1 = series1.bullets.push(new am4charts.CircleBullet());
        /*series1.heatRules.push({
            target: bullet1.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet1.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";

        /*var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueX = "x";
        series2.dataFields.valueY = "by";
        series2.dataFields.value = "bValue";
        series2.strokeWidth = 2;*/

       // var bullet2 = series2.bullets.push(new am4charts.CircleBullet());
        /*series2.heatRules.push({
            target: bullet2.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet2.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";

//scrollbars

// Add scrollbar
chart.scrollbarX = new am4charts.XYChartScrollbar();
chart.scrollbarX.series.push(series1);

// Add cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.xAxis = xAxis;
chart.cursor.snapToSeries = series1;
        /*chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarY = new am4core.Scrollbar();*/

    });
    am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
        var chart = am4core.create("chartdiv_5", am4charts.XYChart);
          
// Add data
        chart.data = data_rms
    
// Create axes
        var xAxis = chart.xAxes.push(new am4charts.ValueAxis());
        xAxis.renderer.minGridDistance = 40;

// Create value axis
        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueX = "key";
        series1.dataFields.valueY = "rms";

        //var bullet1 = series1.bullets.push(new am4charts.CircleBullet());
        /*series1.heatRules.push({
            target: bullet1.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet1.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";

        /*var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueX = "x";
        series2.dataFields.valueY = "by";
        series2.dataFields.value = "bValue";
        series2.strokeWidth = 2;*/

       // var bullet2 = series2.bullets.push(new am4charts.CircleBullet());
        /*series2.heatRules.push({
            target: bullet2.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet2.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";

//scrollbars

// Add scrollbar
chart.scrollbarX = new am4charts.XYChartScrollbar();
chart.scrollbarX.series.push(series1);

// Add cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.xAxis = xAxis;
chart.cursor.snapToSeries = series1;
        /*chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarY = new am4core.Scrollbar();*/

    });

}



JS;
//$this->registerJs($js);
?>
<!-- Styles -->
<style>
    #chartdiv_ {
        width: 100%;
        height: 500px;
        max-width: 100%;
        background-color: white;
    }
    #chartdiv_2 {
        width: 100%;
        height: 500px;
        max-width: 100%;
        background-color: white;
    }
    .back{
        background-color: white;
    }

</style>



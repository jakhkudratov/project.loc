<?php
namespace frontend\modules\signal\controllers;

use backend\models\SignalGroup;
use backend\models\Signals;
use backend\models\SignalsSearch;
use common\models\FftForm;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\SignalForm;
use frontend\models\VerifyEmailForm;
use PHPExcel_IOFactory;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter; 
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\LessonForm;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\UploadedFile;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            /*'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],*/
            /*[
               'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ], 
            ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($id)
    {
        $model = new SignalForm();
        Yii::$app->view->params['user_id'] = $id;
        return $this->render('index', [
            'model' => $model
        ]);
    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionSsi()
    {


        return $this->render('ssi', [

        ]);
    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionRms()
    {


        return $this->render('rms', [

        ]);
    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIemg()
    {


        return $this->render('iemg', [

        ]);
    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionMav()
    {


        return $this->render('mav', [

        ]);
    }

    /**
     * Upload Exel File.
     * @return array
     */
    public function actionUploadFile()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = [];

        if ($file_image = UploadedFile::getInstancesByName('file')) {
            foreach ($file_image as $file) {
                $folder = '/test/';
                if (!file_exists(Yii::getAlias('@rootDir').'/uploads/')) {
                    mkdir(Yii::getAlias('@rootDir').'/uploads/', 0777, true);
                }
                $ext = pathinfo($file->name, PATHINFO_EXTENSION);
                $name = pathinfo($file->name, PATHINFO_FILENAME);
                $generateName = Yii::$app->security->generateRandomString();
                $path = Yii::getAlias('@uploadsPath') . $folder . $generateName . ".{$ext}";
                $file->saveAs($path);
                //dd($path);
                $data = [
                    'generate_name' => $generateName,
                    'name' => $name,
                    'path' => Yii::getAlias('@uploadsUrl') . $folder . $generateName . ".{$ext}"
                ];
            }
        }

        return $data;
    }

    /**
     * Upload Product Image.
     *
     */
    public function actionUploadImage()
    {
        $data = [];

        if ($file_image = UploadedFile::getInstancesByName('image')) {
            foreach ($file_image as $file) {
                $folder = '/product/images/';
                $ext = pathinfo($file->name, PATHINFO_EXTENSION);
                $name = pathinfo($file->name, PATHINFO_FILENAME);
                $generateName = Yii::$app->security->generateRandomString();
                $path = Yii::getAlias('@uploadsPath') . $folder . $generateName . ".{$ext}";
                $file->saveAs($path);
                $data = [
                    'generate_name' => $generateName,
                    'name' => $name,
                    'path' => Yii::getAlias('@uploadsUrl') . $folder . $generateName . ".{$ext}"
                ];
            }
        }

        return json_decode($data);
    }


    /**
     * Delete Product Image.
     * @return object
     */
    public function actionDeleteImage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ($post = Yii::$app->request->post()) ? $post['key'] : null;
    }

    public function actionImageImport()
    {

        if ($post = Yii::$app->request->post()) {
            $excel = Yii::getAlias('@rootDir') . $post['name_zip'];

            try {
                $inputFileType = PHPExcel_IOFactory::identify($excel);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($excel);
            } catch (\Exception $e) {
                die('Error');
            }
            //var_dump($objPHPExcel);die();
            $model = new Signals();
            $model->importFromExcel($objPHPExcel);

            /*if (ExcelImportService::importProductElements($objPHPExcel))
                return $this->redirect(['index']);*/
        }

        return $this->redirect(['index']);
    }

    public function actionSetData()
    {
        //Yii::$app->controller->enableCsrfValidation = false;
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax || Yii::$app->request->isPost)
        {

            $signals = Signals::find()->select(['time', 'amplituda'])->asArray()->orderBy(['id' => SORT_ASC])->all();
            $data = [];
            $data_mav = [];
            $data_ssi = [];
            $data_rms = [];
            $key = 0;
            $my_sgnal = array_chunk($signals, 100);
            foreach ($my_sgnal as $signal)
            {
                $qiymat = 0;
                $qiymat_ssi = 0;
                foreach ($signal as $item) {
                    $qiymat += abs($item['amplituda']);
                    $qiymat_ssi += pow(2, abs($item['amplituda']));
                }
                $data[] =[
                    'key' => $key,
                    'emp' => $qiymat
                ];
                $data_mav[] =[
                    'key' => $key,
                    'emp' => $qiymat/100
                ];
                $data_ssi[] =[
                    'key' => $key,
                    'ssi' => $qiymat_ssi
                ];
                $data_rms[] =[
                    'key' => $key,
                    'rms' => sqrt($qiymat_ssi/100)
                ];
                $key ++;
            }

            return [
                'xAxis' => ArrayHelper::map($signals, 'time', 'time'),
                'yAxis' => ArrayHelper::map($signals, 'amplituda', 'amplituda'),
                'data' => $data,
                'new_data' => $signals,
                'mav' => $data_mav,
                'data_ssi' => $data_ssi,
                'data_rms' => $data_rms
            ];
        }
        return null;

    }

    public function actionData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax)
        {

            $signals = Signals::find()->select(['time', 'amplituda'])->asArray()->orderBy(['id' => SORT_ASC])->limit(7000)->all();
            $data = [];
            $data_mav = [];
            $data_ssi = [];
            $data_rms = [];
            $key = 0;
            $my_sgnal = array_chunk($signals, 100);
            foreach ($my_sgnal as $signal)
            {
                $qiymat = 0;
                $qiymat_ssi = 0;
                foreach ($signal as $item) {
                    $qiymat += abs($item['amplituda']);
                    $qiymat_ssi += pow(2, abs($item['amplituda']));
                }
                $data[] =[
                    'key' => $key,
                    'emp' => $qiymat
                ];
                $data_mav[] =[
                    'key' => $key,
                    'emp' => $qiymat/100
                ];
                $data_ssi[] =[
                    'key' => $key,
                    'ssi' => $qiymat_ssi
                ];
                $data_rms[] =[
                    'key' => $key,
                    'rms' => sqrt($qiymat_ssi/100)
                ];
                $key ++;
            }

            return [
                'xAxis' => ArrayHelper::map($signals, 'time', 'time'),
                'yAxis' => ArrayHelper::map($signals, 'amplituda', 'amplituda'),
                'data' => $data,
                'new_data' => $signals,
                'mav' => $data_mav,
                'data_ssi' => $data_ssi,
                'data_rms' => $data_rms
            ];
        }
    }



    public function actionDataIemg()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax)
        {

            $signals = Signals::find()->select(['time', 'amplituda'])->asArray()->orderBy(['id' => SORT_ASC])->all();
            $data = [];
            $key = 0;
            $my_sgnal = array_chunk($signals, 100);
            foreach ($my_sgnal as $signal)
            {
                $qiymat = 0;
                foreach ($signal as $item) {
                    $qiymat += abs($item['amplituda']);
                }
                $data[] =[
                    'key' => $key,
                    'emp' => $qiymat
                ];
                $key++;
            }

            return [
                'data' => $data,
            ];
        }
        return null;
    }



    public function actionDataRms()
    {
        //Yii::$app->controller->enableCsrfValidation = false;
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax || Yii::$app->request->isPost)
        {

            $signals = Signals::find()->select(['time', 'amplituda'])->asArray()->orderBy(['id' => SORT_ASC])->all();
            $data = [];
            $data_mav = [];
            $data_ssi = [];
            $data_rms = [];
            $key = 0;
            $my_sgnal = array_chunk($signals, 100);
            foreach ($my_sgnal as $signal)
            {
                $qiymat_ssi = 0;
                foreach ($signal as $item) {
                    $qiymat_ssi += pow(2, abs($item['amplituda']));
                }

                $data_rms[] =[
                    'key' => $key,
                    'rms' => sqrt($qiymat_ssi/100)
                ];
                $key ++;
            }

            return [

                'data_rms' => $data_rms
            ];
        }
        return null;
    }



    public function actionDataSsi()
    {
        //Yii::$app->controller->enableCsrfValidation = false;
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax || Yii::$app->request->isPost)
        {

            $signals = Signals::find()->select(['time', 'amplituda'])->asArray()->orderBy(['id' => SORT_ASC])->all();
            $data = [];
            $data_mav = [];
            $data_ssi = [];
            $data_rms = [];
            $key = 0;
            $my_sgnal = array_chunk($signals, 100);
            foreach ($my_sgnal as $signal)
            {
                $qiymat_ssi = 0;
                foreach ($signal as $item) {
                    $qiymat_ssi += pow(2, abs($item['amplituda']));
                }

                $data_ssi[] =[
                    'key' => $key,
                    'ssi' => $qiymat_ssi
                ];
                $key ++;
            }

            return [
                'data_ssi' => $data_ssi,
            ];
        }
        return null;
    }



    public function actionDataMav()
    {
        //Yii::$app->controller->enableCsrfValidation = false;
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax || Yii::$app->request->isPost)
        {

            $signals = Signals::find()->select(['time', 'amplituda'])->asArray()->orderBy(['id' => SORT_ASC])->all();
            $data_mav = [];
            $key = 0;
            $my_sgnal = array_chunk($signals, 100);
            foreach ($my_sgnal as $signal)
            {
                foreach ($signal as $item) {
                    $qiymat += abs($item['amplituda']);
                }

                $data_mav[] =[
                    'key' => $key,
                    'mav' => $qiymat/100
                ];
                $key ++;
            }

            return [
                'data_mav' => $data_mav
            ];
        }
        return null;
    }



    public function actionAlldata()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax || Yii::$app->request->isPost)
        {

            $signals = Signals::find()->select(['time', 'amplituda'])->asArray()->orderBy(['id' => SORT_ASC])->all();

            return [

                'new_data' => $signals,
            ];
        }
    }
    public function actionFirstSignal()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax || Yii::$app->request->isPost)
        {

            $signals = Signals::find()->select(['time', 'amplituda'])->asArray()->orderBy(['id' => SORT_ASC])->all();

            return [

                'new_data' => $signals,
            ];
        }
    }


    public function actionAll()
    {
        return $this->render('all');
    }
    public function actionEpmg()
    {
        return $this->render('echart');
    }
    public function actionWl()
    {
        return $this->render('wl');
    }

    public function actionAac()
    {
        return $this->render('aac');
    }
    public function actionLog()
    {
        return $this->render('log');
    }

    public function actionMytest()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $signals = Signals::find()->asArray()->all();
        $array = ArrayHelper::map($signals, 'id', 'amplituda');
        $my_data = [];
        foreach ($array as $arr)
        {
            $my_data[]=abs($arr);
        }

        $data = FftForm::Fourier($my_data, -1);
        $data_1 = [];
        $key =0;
        foreach ($data as $signal)
        {
            $data_1[] =[
                'time' => $key,
                'amplituda' => abs($signal)
            ];
            $key++;
        }
        return [
            'new_data' => $data_1
        ];
    }

    public function actionSetFft()
    {

        Yii::$app->response->format = Response::FORMAT_JSON;
        if (true)
        {

            $signals = Signals::find()->select(['id', 'amplituda'])->asArray()->orderBy(['id' => SORT_ASC])->limit(1024)->all();
            echo '<pre>';
            //var_dump(ArrayHelper::map($signals, 'id', 'amplituda'));
            $data_mav = [];
            $my_form = new FftForm();
            $my_data = $my_form->my_fft(ArrayHelper::map($signals, 'id', 'amplituda'), 1);
            var_dump($my_data);
            die();
            return [
                'my_data' => $my_data
            ];
        }

    }


    public function actionSetWl()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $signal_group = SignalGroup::find()->orderBy(['id'=> SORT_DESC])->one();
        $signals = Signals::find()->where(['group_id' => $signal_group->id])->asArray()->all();
        $my_signals = array_chunk($signals, 100);
        $data = [];
        $key = 0;
        //echo '<pre>';
       // var_dump($signals);
        foreach ($my_signals as $my_signal) {
            $qiymat = 0;
            $my_key = 0;
            foreach ($my_signal as $signal)
            {
                if ($my_key=== 98)
                    break;
                $signal_1 = $my_signal[$my_key + 1];
                //var_dump($signal_1);
                //var_dump($signal);
                $qiymat += abs($signal_1['amplituda'] - $signal['amplituda']);
                $my_key ++;
            }
            $data[] =[
                'key' => $key,
                'value' => $qiymat
            ];
            $key++;
        }
        return [
            'data' => $data
        ];

    }

    public function actionSetAac()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $signal_group = SignalGroup::find()->orderBy(['id'=> SORT_DESC])->one();
        $signals = Signals::find()->where(['group_id' => $signal_group->id])->asArray()->all();
        $my_signals = array_chunk($signals, 100);
        $data = [];
        $key = 0;
        //echo '<pre>';
       // var_dump($signals);
        foreach ($my_signals as $my_signal) {
            $qiymat = 0;
            $my_key = 0;
            foreach ($my_signal as $signal)
            {
                if ($my_key ===98)
                    break;
                $signal_1 = $my_signal[$my_key + 1];
                //var_dump($signal_1);
                //var_dump($signal);
                $qiymat += abs($signal_1['amplituda'] - $signal['amplituda']);
                $my_key ++;
            }
            $data[] =[
                'key' => $key,
                'value' => $qiymat/100
            ];
            $key++;
        }
        return [
            'data' => $data
        ];

    }
    public function actionSetLog()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $signal_group = SignalGroup::find()->orderBy(['id'=> SORT_DESC])->one();
        $signals = Signals::find()->where(['group_id' => $signal_group->id])->asArray()->all();
        $my_signals = array_chunk($signals, 100);
        $data = [];
        $key = 0;
        //echo '<pre>';
       // var_dump($signals);
        foreach ($my_signals as $my_signal) {
            $qiymat = 0;
            foreach ($my_signal as $signal)
            {
                $qiymat += log(abs($signal['amplituda']));

            }
            $data[] =[
                'key' => $key,
                'value' => exp($qiymat/100)
            ];
            $key++;
        }
        return [
            'data' => $data
        ];

    }
}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SignalsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Signals';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="signals-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



</div>


<?php
$js = <<<JS

sendAjaxToCart();


function sendAjaxToCart(){
    $.ajax({
        //method: "POST",
        url: "/site/data-rms", 
        success: function (response){
            //console.log(response);
            if (!response === false){
                painChart(response['data_rms'])
                //console.log(response)
            }
        }

    });
}



function painChart(data_rms)
{
  
     
    am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
        var chart = am4core.create("chartdiv_5", am4charts.XYChart);
          
// Add data
        chart.data = data_rms
    
// Create axes
        var xAxis = chart.xAxes.push(new am4charts.ValueAxis());
        xAxis.renderer.minGridDistance = 40;

// Create value axis
        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
        var series1 = chart.series.push(new am4charts.LineSeries());
        series1.dataFields.valueX = "key";
        series1.dataFields.valueY = "rms";

        //var bullet1 = series1.bullets.push(new am4charts.CircleBullet());
        /*series1.heatRules.push({
            target: bullet1.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet1.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";

        /*var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueX = "x";
        series2.dataFields.valueY = "by";
        series2.dataFields.value = "bValue";
        series2.strokeWidth = 2;*/

       // var bullet2 = series2.bullets.push(new am4charts.CircleBullet());
        /*series2.heatRules.push({
            target: bullet2.circle,
            min: 5,
            max: 20,
            property: "radius"
        });*/

        //bullet2.tooltipText = "{valueX} x {valueY}: [bold]{value}[/]";

//scrollbars

// Add scrollbar
chart.scrollbarX = new am4charts.XYChartScrollbar();
chart.scrollbarX.series.push(series1);

// Add cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.xAxis = xAxis;
chart.cursor.snapToSeries = series1;
        /*chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarY = new am4core.Scrollbar();*/

    });

}



JS;
$this->registerJs($js);
?>


<!-- HTML -->
<div class="back">

    <h2>RMS</h2>
    <div id="chartdiv_5" style="height: 1000px"></div>
</div>

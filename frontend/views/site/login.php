<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="container" id="container">

        <div class="form-container sign-in-container">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <h1>Sign in</h1>
                <div class="social-container">
                    <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
                    <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
                </div>
                <span>or use your account</span>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'my-input', 'placeholder' => 'username'])->label(false) ?>

                <?= $form->field($model, 'password')->passwordInput(['class' => 'my-input', 'placeholder' => 'password'])->label(false) ?>

                <a href="#">Forgot your password?</a>

                <div class="form-group">
                    <?= Html::submitButton('Login', [ 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>

        </div>
        <div class="overlay-container">
            <div class="overlay">
                <div class="overlay-panel overlay-right">
                    <h1>Hello, Friend!</h1>
                    <p>Enter your personal details and start journey with us</p>
                </div>
            </div>
        </div>
    </div>

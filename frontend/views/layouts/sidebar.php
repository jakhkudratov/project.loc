<?php

use yii\helpers\Url;

?>
<div class="sidebar" data-image="../assets/img/sidebar-5.jpg">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="/" class="simple-text">
                213-17 Group
            </a>
        </div>
        <ul class="nav">
            <li>
                <a class="nav-link active" href="<?= Url::to(['/site/index'])?>">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>Main Page</p>
                </a>
            </li>
            <li>
                <a class="nav-link active" href="<?= Url::to(['/site/all'])?>">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>First Signal</p>
                </a>
            </li>

            <li>
                <a class="nav-link" href="<?= Url::to(['/site/iemg'])?>">
                    <i class="nc-icon nc-circle-09"></i>
                    <p>IEMG</p>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= Url::to(['/site/ssi'])?>">
                    <i class="nc-icon nc-notes"></i>
                    <p>SSI</p>
                </a>
            </li>
            <li>
                <a class="nav-link" href="<?= Url::to(['/site/mav'])?>">
                    <i class="nc-icon nc-paper-2"></i>
                    <p>MAV</p>
                </a>
            </li>
            <li>
                <a class="nav-link" href="<?= Url::to(['/site/rms'])?>">
                    <i class="nc-icon nc-atom"></i>
                    <p>RMS</p>
                </a>
            </li>
            <li>
                <a class="nav-link" href="<?= Url::to(['/site/wl'])?>">
                    <i class="nc-icon nc-atom"></i>
                    <p>WL</p>
                </a>
            </li>
            <li>
                <a class="nav-link" href="<?= Url::to(['/site/aac'])?>">
                    <i class="nc-icon nc-atom"></i>
                    <p>AAC</p>
                </a>
            </li>
            <li>
                <a class="nav-link" href="<?= Url::to(['/site/log'])?>">
                    <i class="nc-icon nc-atom"></i>
                    <p>LOG</p>
                </a>
            </li>

            <li class="nav-item active active-pro">
                <a class="nav-link active" href="https://t.me/tuituz_official" target="_blank">
                    <i class="nc-icon nc-send"></i>
                    <p>Telegram</p>
                </a>
            </li>
        </ul>
    </div>
</div>

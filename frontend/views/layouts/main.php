<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

\frontend\assets\AppAsset_2::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

        <!-- Styles -->
        <style>
            #chartdiv_ {
                width: 100%;
                height: 500px;
                max-width: 100%;
                background-color: white;
            }
            #chartdiv_2 {
                width: 100%;
                height: 500px;
                max-width: 100%;
                background-color: white;
            }
            .back{
                background-color: white;
            }

        </style>
    </head>
    <body>
    <?php $this->beginBody() ?>


    <div class="wrapper">

        <?= $this->render('sidebar')?>
        <div class="main-panel">
            <?= $this->render('header')?>
            <!-- Navbar -->
            <!-- End Navbar -->
            <div class="content">
                <?= $content ?>

            </div>
            <?= $this->render('footer')?>

        </div>
    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
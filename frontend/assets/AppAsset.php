<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';

    public $js = [
        'https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js',
        'public/js/core/popper.min.js',
        'public/js/core/bootstrap.min.js',
        'public/js/plugins/bootstrap-switch.js',
        'public/js/light-bootstrap-dashboard.js',
        'public/js/demo.js',
        'https://cdn.amcharts.com/lib/4/core.js',
        'https://cdn.amcharts.com/lib/4/charts.js',
        'https://cdn.amcharts.com/lib/4/themes/animated.js',
        'https://cdn.amcharts.com/lib/4/themes/material.js',
        'https://cdn.jsdelivr.net/npm/fourier/fourier.min.js',
        'js/echarts.min.js'



    ];
    public $baseUrl = '@web';
    public $css = [
        //'public/css/bootstrap.min.css',
        'public/css/light-bootstrap-dashboard.css',
        'public/css/demo.css'
    ];
    public $depends = [
        'yii\bootstrap4\BootstrapPluginAsset',
        'yii\web\YiiAsset',
    ];
}

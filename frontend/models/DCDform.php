<?php


namespace frontend\models;


class DCDform
{
    function dct1D($in) {
        $results = array();
        $N = count($in);
        for ($k = 0; $k < $N; $k++) {
            $sum = 0;
            for ($n = 0; $n < $N; $n++) {
                $sum += $in[$n] * cos($k * pi() * ($n + 0.5) / ($N));
            }
            $sum *= sqrt(2 / $N);
            if ($k == 0) {
                $sum *= 1 / sqrt(2);
            }
            $results[$k] = $sum;
        }
        return $results;
    }

}
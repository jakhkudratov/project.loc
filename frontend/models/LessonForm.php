<?php

namespace frontend\models;

use Yii;
use common\models\User;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use common\models\WeekDays;
use common\models\Rooms;
use common\models\Para;
use common\models\ScienceTable;
use common\models\Lesson;
use common\models\TeachScience;
use common\models\Teacher;
use common\models\Groups;
use common\models\Science;
use yii\helpers\ArrayHelper;
use common\models\Student;

class LessonForm extends Model
{
    /**
     * @var string
     */
    public $day;
    public $time;
    public $room;


     public function rules()
    {
        return [
            // name, email, subject and body are required
            [['day'], 'required'],
        ];
    }

    public function getData()
    {

        $days = WeekDays::find()->all();
        $rooms = Rooms::find()->all();
        $paras = Para::find()->all();

        return [
            'days' => $days,
            'rooms' => $rooms,
            'paras' => $paras
        ];
    }

    public function getDatasOne()
    {
        $week_day = $this->getWeekDay();
        $week_day = WeekDays::find()
            ->where([
                    'day_name' => $week_day
                ])
            ->one();
        $para = Para::find()
            ->where([
                    '<', 'begin_time' , $this->time
                ])
            ->andWhere([
                    '>', 'end_time' , $this->time
                ])
            ->one();
        $room = Rooms::findOne($this->room);

        if (empty($week_day)) {
            return false;
        }
        else
        {
            if ($this->time == 0 && $this->room == 0) {
                $lesson = ScienceTable::find()
                ->where([
                    'week_day_id' => $week_day->id
                ])->all();    
            }
            elseif ($this->room == 0) {
                $lesson = ScienceTable::find()
                    ->where([
                        'para_id' => $para->id,
                        'week_day_id' => $week_day->id
                    ])->all();
            }
            elseif ($this->time == 0) {
                $lesson = ScienceTable::find()
                    ->where([
                        'room_id' => $room->id,
                        'week_day_id' => $week_day->id
                    ])->all(); 
            }
            
            else
            {
                $lesson = ScienceTable::find()
                ->where([
                    'room_id' => $room->id,
                    'para_id' => $para->id,
                    'week_day_id' => $week_day->id
                ])->one();
            }
            
            return $lesson;
        }

        return false;
    }

    public function getWeekDay()
    {
        //return $this->day;
        $dayOfweek = date('D', strtotime($this->day));
        $array = [
            'Thu' => 'payshanba',
            'Tue' => 'seshanba',
            'Mon' => 'dushanba',
            'Wed' => 'chorshanba',
            'Fri' => 'juma',
            'Sat' => 'shanba'


        ];

        return $array[$dayOfweek];
    }


    public function returnDatas()
    {

        $data = $this->getDatasOne();

        if ($data === false ||$data === null) {
            return false;
        }
        if (empty($data)) {
            return false;
        }
        if (is_array($data) && count($data) > 1) {
            $return = [];
            foreach ($data as $dat) {
                $lesson = Lesson::findOne($dat->lesson_id);
                $teachScience = TeachScience::findOne($lesson->teach_science_id);
                $group = Groups::findOne($lesson->group_id);
                $teacher = Teacher::findOne($teachScience->teacher_id);
                if ($teacher->user_id !== Yii::$app->user->id) {
                    continue;
                }
                $science = Science::findOne($teachScience->science_id);
                $return['teacher'][] = $teacher->full_name;
                $return['group'][] = $group->group_namber;
                $return['science'][] = $science->science_name;

                $return['room'][] = $dat->room->room_namber;
                $return['para'][] = $dat->para->number_para;

            }
            if (empty($return)) {
                return false;
            }
            return $return;
        }
        if (is_array($data) && count($data) === 1) {
            $lesson = Lesson::findOne($data['0']->lesson_id);
            $teachScience = TeachScience::findOne($lesson->teach_science_id);
            $group = Groups::findOne($lesson->group_id);
            $teacher = Teacher::findOne($teachScience->teacher_id);
            $science = Science::findOne($teachScience->science_id);
    
            return [
                'teacher' => $teacher->full_name,
                'group' => $group->group_namber,
                'science' => $science->science_name,
                'room' => $data['0']->room->room_namber,
                'para' => $data['0']->para->number_para
            ];
        }
        $lesson = Lesson::findOne($data->lesson_id);
        $teachScience = TeachScience::findOne($lesson->teach_science_id);
        $group = Groups::findOne($lesson->group_id);
        $teacher = Teacher::findOne($teachScience->teacher_id);
        $science = Science::findOne($teachScience->science_id);

        return [
            'teacher' => $teacher->full_name,
            'group' => $group->group_namber,
            'science' => $science->science_name,
            'room' => $data->room->room_namber,
            'para' => $data->para->number_para
        ];
    }


    public function getStudentLessons()
    {
        $user_id = Yii::$app->user->id;
        $student = Student::find()
            ->where([
                    'user_id' => $user_id
                ])
            ->one();
        $lessons = Lesson::find()
            ->where([
                    'group_id' => $student->group_id
                ])
            ->all();
        $ids = ArrayHelper::map($lessons, 'id', 'id');
        $week_day = $this->getWeekDay();
        $week_day = WeekDays::find()
            ->where([
                    'day_name' => $week_day
                ])
            ->one();
        $tables = ScienceTable::find()
                    ->where([
                        'week_day_id' => $week_day->id
                    ])
                    ->andWhere([
                        'lesson_id' => $ids
                    ])
                    ->all();
        $return = [];

        foreach ($tables as $table) {
            $lesson = Lesson::findOne($table->lesson_id);
            $teachScience = TeachScience::findOne($lesson->teach_science_id);
            $group = Groups::findOne($lesson->group_id);
            $teacher = Teacher::findOne($teachScience->teacher_id);
           
            $science = Science::findOne($teachScience->science_id);
            $return['teacher'][] = $teacher->full_name;
            $return['group'][] = $group->group_namber;
            $return['science'][] = $science->science_name;

            $return['room'][] = $table->room->room_namber;
            $return['para'][] = $table->para->number_para;
        }
        //var_dump($return);die;
        return $return;
    }
    
}

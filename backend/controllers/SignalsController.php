<?php

namespace backend\controllers;

use PHPExcel_IOFactory;
use Yii;
use backend\models\Signals;
use backend\models\SignalsSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;
use function Couchbase\defaultDecoder;

/**
 * SignalsController implements the CRUD actions for Signals model.
 */
class SignalsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Signals models.
     * @return mixed
     */
    public function actionIndex()
    {

        $this->layout = 'main-login';
        $searchModel = new SignalsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all Signals models.
     * @return mixed
     */
    public function actionIndex2()
    {

        $this->layout = 'main-login';
        $searchModel = new SignalsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index_2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Signals model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Signals model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Signals();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Signals model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Signals model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Signals model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Signals the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Signals::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Upload Exel File.
     * @return array
     */
    public function actionUploadFile()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = [];

        if ($file_image = UploadedFile::getInstancesByName('file')) {
            foreach ($file_image as $file) {
                $folder = '/test/';
                if (!file_exists(Yii::getAlias('@rootDir').'/uploads/')) {
                    mkdir(Yii::getAlias('@rootDir').'/uploads/', 0777, true);
                }
                $ext = pathinfo($file->name, PATHINFO_EXTENSION);
                $name = pathinfo($file->name, PATHINFO_FILENAME);
                $generateName = Yii::$app->security->generateRandomString();
                $path = Yii::getAlias('@uploadsPath') . $folder . $generateName . ".{$ext}";
                $file->saveAs($path);
                //dd($path);
                $data = [
                    'generate_name' => $generateName,
                    'name' => $name,
                    'path' => Yii::getAlias('@uploadsUrl') . $folder . $generateName . ".{$ext}"
                ];
            }
        }

        return $data;
    }

    /**
     * Upload Product Image.
     *
     */
    public function actionUploadImage()
    {
        $data = [];

        if ($file_image = UploadedFile::getInstancesByName('image')) {
            foreach ($file_image as $file) {
                $folder = '/product/images/';
                $ext = pathinfo($file->name, PATHINFO_EXTENSION);
                $name = pathinfo($file->name, PATHINFO_FILENAME);
                $generateName = Yii::$app->security->generateRandomString();
                $path = Yii::getAlias('@uploadsPath') . $folder . $generateName . ".{$ext}";
                $file->saveAs($path);
                $data = [
                    'generate_name' => $generateName,
                    'name' => $name,
                    'path' => Yii::getAlias('@uploadsUrl') . $folder . $generateName . ".{$ext}"
                ];
            }
        }

        return json_decode($data);
    }


    /**
     * Delete Product Image.
     * @return object
     */
    public function actionDeleteImage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ($post = Yii::$app->request->post()) ? $post['key'] : null;
    }

    public function actionImageImport()
    {

        if ($post = Yii::$app->request->post()) {
            $excel = Yii::getAlias('@rootDir') . $post['name_zip'];

            try {
                $inputFileType = PHPExcel_IOFactory::identify($excel);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($excel);
            } catch (\Exception $e) {
                die('Error');
            }
            //var_dump($objPHPExcel);die();
            $model = new Signals();
            $model->importFromExcel($objPHPExcel);

            /*if (ExcelImportService::importProductElements($objPHPExcel))
                return $this->redirect(['index']);*/
        }

        return $this->redirect(['index']);
    }

    public function actionSetData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (true)

        {
            $signals = Signals::find()->select(['time', 'amplituda'])->asArray()->orderBy(['id' => SORT_ASC])->all();
            $data = [];
            $data_mav = [];
            $data_ssi = [];
            $data_rms = [];
            $key = 0;
            $my_sgnal = array_chunk($signals, 100);
            foreach ($my_sgnal as $signal)
            {
                $qiymat = 0;
                $qiymat_ssi = 0;
                foreach ($signal as $item) {
                    $qiymat += abs($item['amplituda']);
                    $qiymat_ssi += pow(2, abs($item['amplituda']));
                }
                $data[] =[
                    'key' => $key,
                    'emp' => $qiymat
                ];
                $data_mav[] =[
                    'key' => $key,
                    'emp' => $qiymat/100
                ];
                $data_ssi[] =[
                    'key' => $key,
                    'ssi' => $qiymat_ssi
                ];
                $data_rms[] =[
                    'key' => $key,
                    'rms' => sqrt($qiymat_ssi/100)
                ];
                //$data[]['emp'] = $qiymat;
                $key ++;
            }
            /*foreach ($signals as $signal)
            {
               $data[] = abs($value);
            }*/


            //var_dump(ArrayHelper::map($signals, 'amplituda', 'amplituda'));
            //var_dump($data);

            //die();
           /* echo '<pre>';
            var_dump($data);
            die();*/
            return [
                'xAxis' => ArrayHelper::map($signals, 'time', 'time'),
                'yAxis' => ArrayHelper::map($signals, 'amplituda', 'amplituda'),
                'data' => $data,
                'new_data' => $signals,
                'mav' => $data_mav,
                'data_ssi' => $data_ssi,
                'data_rms' => $data_rms
            ];
        }
        return null;
    }



}

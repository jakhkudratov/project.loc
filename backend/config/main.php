<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'name' => 'My university',
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'admin' => [
            'class' => 'backend\modules\admin\Module',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'idField' => 'id',
                    'usernameField' => 'username',
                ],
            ],
//            'layout' => 'right-menu',
            //'layout' => '@backend/modules/admin/views/layouts/main.php',
        ],
        'translatemanager' => [
            'class' => 'backend\modules\translatemanager\Module',
            'allowedIPs' => ['*',],
            'ignoredCategories' => [
                'yii',
                'language',
                'rbac-admin',
                'kvbase',
                'kvselect',
            ],
//            'tables' => [
//                [
//                    'connection' => 'db',
//                    'table' => '{{%menus}}',
//                    'columns' => ['name', 'description'],
//                    'category' => 'database-table-name',
//                ],
//                [
//                    'connection' => 'db',
//                    'table' => '{{%menu_items}}',
//                    'columns' => ['label', 'description'],
//                    'category' => 'database-table-name',
//                ],
//            ],
        ],


    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl'=>'/admin',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'scriptUrl'=>'/backend/index.php',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                /*'' => 'site/index',                                
                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',*/
                'teacher/view/<id:\d+>' => 'teacher/view'
            ],
        ],

         /*'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
                ],
            ],
         ],*/

        /*'as access' => [
            'class' => 'yii\filters\AccessControl',
            'except' => ['auth/login', 'site/error'],
            'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],

            ]*/
    ],
    'params' => $params,
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'plugin' => [
                [
                    'class'=>'\mihaildev\elfinder\plugin\Sluggable',
                    'lowercase' => true,
                    'replacement' => '-'
                ]
            ],
            'root' => [
                'baseUrl' => '@uploadsUrl',
                'basePath' => '@uploadsPath',
                'path' => '/',
                'name' => 'Files'
            ],
            'watermark' => [
                'source' => __DIR__ . '/logo.png', // Path to Water mark image
                'marginRight' => 5,          // Margin right pixel
                'marginBottom' => 5,          // Margin bottom pixel
                'quality' => 95,         // JPEG image save quality
                'transparency' => 70,         // Water mark image transparency ( other than PNG )
                'targetType' => IMG_GIF | IMG_JPG | IMG_PNG | IMG_WBMP, // Target image formats ( bit-field )
                'targetMinPixel' => 200         // Target image minimum pixel size
            ]
        ]
    ],
];

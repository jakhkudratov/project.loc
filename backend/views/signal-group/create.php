<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SignalGroup */

$this->title = 'Create Signal Group';
$this->params['breadcrumbs'][] = ['label' => 'Signal Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="signal-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

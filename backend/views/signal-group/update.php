<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SignalGroup */

$this->title = 'Update Signal Group: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Signal Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="signal-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

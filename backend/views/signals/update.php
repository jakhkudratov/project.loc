<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Signals */

$this->title = 'Update Signals: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Signals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="signals-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php


namespace common\models;


use Exception;

class FFT extends FourierTransform
{
    public $reverseTable = array();
    public $sinTable = array();
    public $cosTable = array();

    public function __construct($bufferSize, $sampleRate)
    {
        parent::__construct($bufferSize, $sampleRate);
        $limit = 1;
        $bit = $bufferSize >> 1;
        while ($limit < $bufferSize) {
            for ($i = 0; $i < $limit; $i++) {
                $this->reverseTable[$i + $limit] = $this->reverseTable[$i] + $bit;
            }
            $limit = $limit << 1;
            $bit = $bit >> 1;
        }
        for ($i = 0; $i < $bufferSize; $i++) {
            $this->sinTable[$i] = sin(-M_PI / $i);
            $this->cosTable[$i] = cos(-M_PI / $i);
        }
    }

    public function foward($buffer)
    {
        $k = floor(log($this->bufferSize, 2));
        if (pow(2, $k) !== $this->bufferSize) throw new Exception('Invalid buffer size, must be a power of 2.');
        if ($this->bufferSize !== count($buffer)) throw new Exception('Supplied buffer is not the same size as defined FFT.');

        $halfSize = 1;
        for ($i = 0; $i < $this->bufferSize; $i++) {
            $this->real[$i] = $buffer[$this->reverseTable[$i]];
            $this->imag[$i] = 0;
        }
        while ($halfSize < $this->bufferSize) {
            $phaseShiftReal = $this->cosTable[$halfSize];
            $phaseShiftImag = $this->sinTable[$halfSize];
            $currentPhaseShiftReal = 1;
            $currentPhaseShiftImag = 0;
            for ($fftStep = 0; $fftStep < $halfSize; $fftStep++) {
                while ($fftStep < $this->bufferSize) {
                    $off = $fftStep + $halfSize;
                    $tr = ($currentPhaseShiftReal * $this->real[$off]) - ($currentPhaseShiftImag * $this->imag[$off]);
                    $ti = ($currentPhaseShiftReal * $this->imag[$off]) + ($currentPhaseShiftImag * $this->real[$off]);
                    $this->real[$off] = $this->real[$fftStep] - $tr;
                    $this->imag[$off] = $this->imag[$fftStep] - $ti;
                    $this->real[$fftStep] += $tr;
                    $this->imag[$fftStep] += $ti;
                    $fftStep += $halfSize << 1;
                }
                $tmpReal = $currentPhaseShiftReal;
                $currentPhaseShiftReal = ($tmpReal * $phaseShiftReal) - ($currentPhaseShiftImag * $phaseShiftImag);
                $currentPhaseShiftImag = ($tmpReal * $phaseShiftImag) + ($currentPhaseShiftImag * $phaseShiftReal);
            }
            $halfSize = $halfSize << 1;
        }
        $this->calculateSpectrum();
    }
}
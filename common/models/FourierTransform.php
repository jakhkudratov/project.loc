<?php


namespace common\models;

use Exception;

class FourierTransform
{
    public $bufferSize;
    public $sampleRate;
    public $bandwidth;
    public $spectrum = array();
    public $real = array();
    public $imag = array();
    public $peakBand = 0;
    public $peak = 0;

    public function __construct($bufferSize,$sampleRate){
        $this->bufferSize = $bufferSize;
        $this->sampleRate = $sampleRate;
        $this->bandwidth = 2 / $bufferSize * $sampleRate / 2;
    }

    public function getBandFrequency($index){
        return $this->bandwidth * $index + $this->bandwidth / 2;
    }

    public function calculateSpectrum(){
        $bSi = 2 / $this->bufferSize;
        for($i = 0,$N = $this->bufferSize/2; $i < $N; $i++){
            $rval = $this->real[$i];
            $ival = $this->imag[$i];
            $mag = $bSi * sqrt($rval * $rval + $ival * $ival);
            if($mag > $this->peak){
                $this->peakBand = $i;
                $this->peak = $mag;
            }
            $this->spectrum[$i] = $mag;
        }
    }
}
